[![Header](https://raw.githubusercontent.com/Mytthewx/Mytthewx/main/header_v2.png "Header")](https://gitlab.com/Mytthew)
<!-- [![wakatime](https://wakatime.com/badge/user/71df4a98-8c24-4092-8747-28e159fdd0f8.svg)](https://wakatime.com/@71df4a98-8c24-4092-8747-28e159fdd0f8) -->
<div align="center">
<h1 align="center">Hello, I'm Mytthew <img src="https://media.giphy.com/media/mGcNjsfWAjY5AEZNw6/giphy.gif" width="50"></h1> 
<p align="center"><em>Software Engineer at <a href="https://www.haddad.com/">Haddad Brands</a></p>
<p align="center"> 
	<img src="https://wakatime.com/badge/user/71df4a98-8c24-4092-8747-28e159fdd0f8.svg?style=for-the-badge" alt="mytthewx" />
</p>

<p align="center"><em>I live happily in a small town in Poland. In addition to my full-time job, I try to develop into a full stack developer every day. I am also a big fan of soccer, especially the English league. Every day I participate in discord servers related to programming and spend time with friends. </p>
<p align="center"><img align="center" alt="GIF" src="https://media.giphy.com/media/3ohzdKvLT1DxFxhZAI/giphy.gif" /></p><br>

<h3 align="center">You can find me here:</h3>
<p align="center"><a href="https://mytthew.eu/">www.mytthew.eu</a><p>
<p align="center"> <img src="https://dcbadge.vercel.app/api/shield/330678278715080704" /> </p>
<p align="center">
</p>

<h3 align="center">Languages and Tools:</h3>
<p align="center"> 
<img src="https://img.shields.io/badge/HTML-239120?style=for-the-badge&logo=html5&logoColor=white" alt="HTML">
<img src="https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white" alt="CSS">
<img src="https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white" alt="SASS">
<img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black" alt="JavaScript">
<img src="https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white" alt=".NET">
<img src="https://img.shields.io/badge/c%23-%23239120.svg?style=for-the-badge&logo=c-sharp&logoColor=white" alt="C#">
<img src="https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white" alt="Java">
<img src="https://img.shields.io/badge/Markdown-000000?style=for-the-badge&logo=markdown&logoColor=white" alt="Markdown">
<img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" alt="React">
<img src="https://img.shields.io/badge/React_Native-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" alt="React native">
<img src="https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white" alt="Bootstrap">
<img src="https://img.shields.io/badge/blazor-%235C2D91.svg?style=for-the-badge&logo=blazor&logoColor=white" alt="Blazor">
<img src="https://img.shields.io/badge/Material--UI-0081CB?style=for-the-badge&logo=material-ui&logoColor=white" alt="MaterialUI">
<img src="https://img.shields.io/badge/-AntDesign-%230170FE?style=for-the-badge&logo=ant-design&logoColor=white" alt="AntDesign">
<img src="https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white" alt="MySQL">
<img src="https://img.shields.io/badge/SQLite-07405E?style=for-the-badge&logo=sqlite&logoColor=white" alt="SQLite">
<img src="https://img.shields.io/badge/Microsoft%20SQL%20Server-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white" alt="MSSQL">
<img src="https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white" alt="Postgres">
<img src="https://img.shields.io/badge/Docker-239120?style=for-the-badge&logo=docker&logoColor=white" alt="Docker">
<img src="https://img.shields.io/badge/yarn-%232C8EBB.svg?style=for-the-badge&logo=yarn&logoColor=white" alt="yarn">
<img src="https://img.shields.io/badge/RESTfull-API-20232A?style=for-the-badge&logo=api&logoColor=white" alt="Restful API">
<img src="https://img.shields.io/badge/JWT-black?style=for-the-badge&logo=JSON%20web%20tokens" alt="JWT">
<img src="https://img.shields.io/badge/mac%20os-000000?style=for-the-badge&logo=macos&logoColor=F0F0F0" alt="macOS">
</p>
</div>
